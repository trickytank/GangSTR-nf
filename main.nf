#!/usr/bin/env nextflow
/* 
======================================================================================
            GangSTR - Nextflow Trial
======================================================================================
*/

/*
======================================================================================
======================================================================================
======                 Parameters                   ======
======================================================================================
======================================================================================
*/

def helpMessage() {

    println """
    Usage:

    --help              To Show this Message
    
    Required arguments:
    --reads             BAM/CRAM file with aligned reads, single 
                    or as a list
    OR
    --csv               CSV file with sample_id, file corresponding
                    to BAM/CRAM files  and index to 
                    corresponding index file

    --ref               Reference Genome
    --regions           Target TR loci (regions) (suffix: .bed)
    --out               Output Prefix

    """.stripIndent()
}

params.help = false

// Show help message
if (params.help) {
    helpMessage()
    exit 0
}

/*
===================================================================
===================================================================
            Default Parameter values
===================================================================
*/

params.reads = false
params.csv = false  // CSV file should have id, file, index and case columns
params.outDir = "results"
params.ref = "$baseDir/../data/GRCh38/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna"
params.regions = "$baseDir/hg38_ver13.bed"
params.out = false
params.chrom = false
/*
 *  Parse the input parameters
 */


if(params.reads && params.csv){
    exit 1, "Specify either --reads or --csv not both!"
} else if(params.reads){
    reads_file = file(params.reads, checkIfExists: true)
    
    // if file does not exist exit with a message
    if (reads_file.isEmpty()) {exit 1, "BAM/CRAM file not found: ${params.reads}" }

    // Get the base name and file extension of the BAM/CRAM files 
    if (reads_file.getClass() == sun.nio.fs.UnixPath) {
        ext = (reads_file.getExtension() == "cram") ? ".crai" : ".bai"
        reads_index = "${reads_file.getParent()}/${reads_file.getBaseName()}.${reads_file.getExtension()}" + "${ext}"
        reads_index = file(reads_index, checkIfExists: true)

        // if file does not exist exit with a message
        if (reads_index.isEmpty()) {exit 1, "BAM/CRAM file not found: ${reads_index.getName()}" }

        Channel
            .of (reads_file.getBaseName() ,reads_file, reads_index).collect()
            .set { reads_ch }
    } else {
        reads_index = []
        for (def index : reads_file) {
            ext = (index.getExtension() == "cram") ? ".crai" : ".bai"
            // if file does not exist exit with a message
            index_f = file("${index.getParent()}/${index.getBaseName()}.${index.getExtension()}" + "${ext}", checkIfExists: true)
            if (index_f.isEmpty()) {exit 1, "BAM/CRAM file not found: ${index_f.getName()}" }
            
            reads_index.add(tuple(index.getSimpleName(), index, index_f))
        }
        reads_ch = Channel.of ( reads_index ).flatten().collate(3)
        //reads_ch.toList().subscribe{ println it }
    }
} else if (params.csv) {

    Channel
        .fromPath(params.csv, checkIfExists: true)
        .ifEmpty { exit 1, "No CSV file found under: ${params.csv}"}
        .splitCsv(header:true)
        .map { row -> tuple(row.id, file(row.file), file(row.index)) }
        .set { reads_ch }
}

ref_file = file(params.ref)
ref_index = file("${ref_file.getParent()}/${ref_file.getName()}" + ".fai", checkIfExists: true)
regions_file = file(params.regions)

process 'gang-profiles' {
    tag "$sample_id"

    cpus 13

    publishDir "${params.outDir}/gangstr-profiles", mode: 'copy', pattern: '*.tar.gz'
    publishDir "${params.outDir}/gangstr-vcf-files", mode: 'copy', pattern: "*.vcf.gz{,.tbi}" 

    input:
        file ref from ref_file
        file ref_i from ref_index
        file regions from regions_file
        tuple sample_id, file(reads), file(reads_index) from reads_ch
    
    output:
        file("*.tar.gz") into out_ch
        tuple sample_id, file("*.vcf*") into vcf_ch
    
    shell:
    """
        # Number of processes
        n_proc=${task.cpus}

        # Make temporary directories
        splitdir="!{sample_id}.split_temp"
        mkdir -p ./\$splitdir

        echo splitdir=\$splitdir 1>&2

        for chrom in chrY chr{15..22} chrX chr{3..14} chr{1..2}
        do
            mkdir -p \$splitdir/\$chrom
            GangSTR --bam  !{reads} \
                --ref  !{ref} \
                --regions !{regions} \
                --chrom \$chrom \
                --out   \$splitdir/\$chrom/!{sample_id} &

            if [ \$(jobs | wc -l) -ge \$n_proc ]; then
                echo "Waiting on jobs to finish before we run more. \$n_proc is current limit." 1>&2
                while [ \$(jobs | wc -l) -ge \$n_proc ];
                do
                    sleep 1
                done
            fi
        done

        echo "Waiting for jobs to finish." 1>&2
        wait

        vcf-concat \$splitdir/**/*.vcf | bgzip -c > ./!{sample_id}.vcf.gz
            tabix -p vcf ./!{sample_id}.vcf.gz &

        rm \$splitdir/**/*.vcf
        tar -czvf ./!{sample_id}.tar.gz \$splitdir
    """
}
