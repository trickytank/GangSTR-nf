# GangSTR

Nextflow pipeline for GangSTR.

This pipeline runs GangSTR on each sample.
Each sample is parallized over chromosomes, with wallclock time of approximately 1 hour per sample with 13 threads.

The pipeline uses a csv file for configuration.
Removing samples, or changes the case/control status of a sample
allow the use of cached profiles (with Nextflow `-resume`).
Only samples added will need to be profiled.

This pipeline is setup to use a software container for GangSTR, such that it does not need to be installed manually. 

# Setup

## Requirements

This pipeline requires a unix-style system (tested in SUSE Linux Enterprise Server 12 SP3).

- [Nextflow](https://www.nextflow.io/) (tested with 19.10.0 build 5170)
- [Singularity](https://singularity.lbl.gov/) (tested with 3.5.3)

A HPC queuing system may help. An example configuration for a Slurm system is included.
The test script setup instructions detail this further.

The pipeline may also run locally on a single server (standard profile). 

GangSTR runs are parallized over chromosomes with 13 threads for each sample.
This pipeline is set up in such a way that the use of more threads per sample is of little benefit, as the time takes at least as long as required for the largest chromosomes.
As this was developed on a server with job number limits, we did not try to run each chromosome as a job but rather each sample as a cluster job.

## Test script

A test script `test.sh` is included that runs the examples from GangSTR. 

You should have a copy of the hg38 human reference genome in FASTA format with a samtools FAI index.
For the test, chromosome sequence identifiers should start with 'chr', for example, chr1, chr15 and chrX.
We have tested this with [GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.gz](ftp://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Eukaryotes/vertebrates_mammals/Homo_sapiens/GRCh38/seqs_for_alignment_pipelines/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.gz) (833 MB).

Download the GangSTR respository from https://github.com/gymreklab/GangSTR for the test data in the `test` directory.

Set the required values in `test.config`. 
This includes the path to the GangSTR/test directory and 
extra command-line arguments to the nextflow script (such as job manager account details).

You will also need to configure `nextflow.config`. 
This file contains two profiles. 
The 'standard' profile runs the Nextflow script on the current machine, while
the 'zeus' profile is set up for a specific HPC system running the Slurm Workload Manager 
(Zeus is located at the Pawsey Supercomputing Centre, Perth WA, Australia). 
Profiles for other systems should be set up with the zeus profile as a guide and process
directives available for executor as documented in <https://www.nextflow.io/docs/latest/executor.html>.
Many of the directives are the same across different HPC executors, though the 
'queue' directive will likely need to be changed for your configuration. 

Run the test with
```
source test.sh
```

Please note that if the pipeline is run with one profile, then changed to another profile,
the pipeline stages may used cached results if the `-resume` option is included,
and is not a true test of the next profile. 
Nextflow will report when cached results are being used and for which stages. 
The `-resume` is useful in large pipelines where you only want to run pipeline jobs that have
not yet been completed. 
To rerun pipeline stages, either run without the `-resume` option (also included in nextflow.config), or delete the directory specified by `workDir` in `nextflow.config`.

# Running on your data

You will need a FASTA file with matching `fai` index. 
The FAI index can be created with samtools (faidx command).

You will need to chose an appropriate GangSTR reference file from
https://github.com/gymreklab/GangSTR#gangstr-reference-files.
This must use the same genome build as your FASTA reference file. 

Create a csv file with at least these columns (with a header row) (in any order):

  - id: ID for output file names. Should not include whitespace.
  - file: path to BAM file, preferably the full absolute path
  - index: path to BAM index file, preferably the full absolute path. (this is required to appropriately link the index file)

Parameters to nextflow may be specified either at the command line or in `nextflow.config`, 
with the command line options taking priority.
```
export REFERENCE="/data_common/GRCh38/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna"
export STUDYNAME="mystudy" #spaces not recommended

nextflow run main.nf \
    --csv samples.csv \
    --regions $GANGSTRTEST/HTT.bed \
    --name "$STUDYNAME" \
    --outdir="results_$STUDYNAME" \
    --reference="$REFERENCE" \
    -profile standard \
    -resume
```

# Limitations

At present, the sex chromosomes are not treated differently for males and females.
At the time of writing, this functionality was
in development in GangSTR not the latest (2.4) GangSTR release.
This may be added in the future as a 'sex' column in the sample csv file.

# Contributors

This Nextflow pipeline was authored by Joseph Sigar and Rick Tankard. 
