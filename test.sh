#!/bin/bash -l

# While this pipeline does not require explicit installation of GangSTR due to Singularity,
# the test data itself comes from the GangSTR Github. 
# https://github.com/gymreklab/GangSTR
# Note that you should either download the release 'Source code' or git-clone the repository,
# as the release package doesn't contain the test directory.
# We use it on your system rather than from the Singularity container to also test that
# you have Singularity set up to read directories outside of the container.

## ---- set these parameters ---- ##

# Path to GangSTR test directory. This is the test directory of the Github repository.
export GANGSTRTEST="/group/pawsey0306/rtankard/software/GangSTR-2.4/test"
# Path to reference FASTA file. Should have an FAI index from samtools.
export REFERENCE="/group/pawsey0306/rtankard/data_common/GRCh38/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna"

# Nextflow run command line. 
# The standard profile runs on the current machine, while a cluster should have its own profile set up.
NEXTFLOW_OPTIONS="-profile standard"


## ---- Testing ---- ##

# Prepare csv for GangSTR test example
cat <(echo "id,file,index") \
    <(for bfile in $GANGSTRTEST/alignment/*bam; 
        do
            echo $(basename $bfile .sorted.bam),$bfile,$bfile.bai
        done
    ) \
    > test_GangSTR.csv

# Run nextflow
nextflow run main.nf \
    $NEXTFLOW_OPTIONS \
    --outdir="results_test_case-control" \
    --name="GangSTR_example" \
    --ref $REFERENCE \
    --regions $GANGSTRTEST/HTT.bed \
    --csv test_GangSTR.csv \
    -resume

